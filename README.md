# My Nvim folder

Setting up neovim takes a bit of effort to get the plugins and config right. To use these settings in
* **Windows 10**: clone this repo into ~/AppData/Local in a folder named `nvim`
* **Linux/Mac**: Clone this repo into ~/.config/nvim folder

This being the first time, the plugins need to be installed. So open `nvim` and enter the command `PlugInstall`. You will need to restart nvim.

To install a new plugin which is in the github repo `https://github.com/username/reponame`, enter the line `Plug 'username/reponame'` into the init.vim file between the lines `call plug#begin()` and `call plug#end()`.

There are two sets of key remaps. One for changing cursor movement to move across visual lines when `j`,`<Up>`,`k`, or `<Down>` are used. The `<Ctrl-O>` key executes one command in normal mode before switching back to the previous mode, so we use it to remap keys in insert mode.

Similarly, I have also remapped key bindings to automatically switch windows using key combinations Alt+{Arrow Key} and Alt+{h,j,k,l} in NORMAL, INSERT, and TERMINAL modes. `tnoremap` will only work in neovim and throw an error in vim. I am not entirely sure.

I will add further instructions as I make changes to the file.
