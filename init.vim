call plug#begin()
Plug 'joshdick/onedark.vim'
Plug 'iCyMind/NeoSolarized'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'scrooloose/NERDTree'
Plug 'donRaphaco/neotex', { 'for': 'tex'}
Plug 'wlangstroth/vim-racket', {'for': 'rkt'}
Plug 'parsonsmatt/intero-neovim', {'for':'hs'}
Plug 'idris-hackers/idris-vim', {'for':'idr'}
Plug 'neovimhaskell/haskell-vim', {'for':'hs'}
Plug 'vim-latex/vim-latex'
Plug 'rafi/awesome-vim-colorschemes'
call plug#end()
"Basic settings
set backspace=2
set backspace=indent,eol,start
syntax on
set number
set showcmd
filetype indent on
set showcmd
set wildmenu
set incsearch
set hlsearch
set linebreak
set autowrite

set foldenable
set foldlevelstart=2
set foldmethod=syntax
set termguicolors
"Indent and wrap
" enable indentation
set breakindent




" Tab keys
set tabstop=4
set softtabstop=0
set shiftwidth=2
set expandtab
set smarttab
" Remap down and up keys to travel up virtual lines
nnoremap j gj
nnoremap <Down> gj
inoremap <Down> <C-o>gj

nnoremap k gk
nnoremap <Up> gk
inoremap <Up> <C-o>gk


colorscheme iceberg  
autocmd vimenter * NERDTree


" Remap Esc in terminal mode to enter normal mode in the terminal emulator
tnoremap <Esc> <C-\><C-n>

" Remap window navigation in terminal mode to Alt +{h,l,j,k}
" <C-\><C-N> to exit insert mode in terminal <C-w> or Ctrl + W to enter window mode
tnoremap <A-h> <C-\><C-N><C-w><Left>
tnoremap <A-l> <C-\><C-N><C-w><Right>
tnoremap <A-k> <C-\><C-N><C-w><Up>
tnoremap <A-j> <C-\><C-N><C-w><Down>

tnoremap <A-Left> <C-\><C-N><C-w><Left>
tnoremap <A-Right> <C-\><C-N><C-w><Right>
tnoremap <A-Up> <C-\><C-N><C-w><Up>
tnoremap <A-Down> <C-\><C-N><C-w><Down>



" Remap window navigation in insert mode as Alt + {h,l,j,k}
inoremap <A-h> <C-\><C-N><C-w><Left>
inoremap <A-l> <C-\><C-N><C-w><Right>
inoremap <A-k> <C-\><C-N><C-w><Up>
inoremap <A-j> <C-\><C-N><C-w><Down>

inoremap <A-Left> <C-\><C-N><C-w><Left>
inoremap <A-Right> <C-\><C-N><C-w><Right>
inoremap <A-Up> <C-\><C-N><C-w><Up>
inoremap <A-Down> <C-\><C-N><C-w><Down>


" Remap window navigation in normal mode as Alt + {h,l,j,k}
nnoremap <A-h> <C-w><Left>
nnoremap <A-l> <C-w><Right>
nnoremap <A-k> <C-w><Up>
nnoremap <A-j> <C-w><Down>

nnoremap <A-Left> <C-\><C-N><C-w><Left>
nnoremap <A-Right> <C-\><C-N><C-w><Right>
nnoremap <A-Up> <C-\><C-N><C-w><Up>
nnoremap <A-Down> <C-\><C-N><C-w><Down>


"Save and Quit shortcuts:
"Save and return to insert mode 
inoremap <C-s> <C-o>:w<cr>
nnoremap <C-s> :w<cr>
" Save and quit
inoremap <C-d> <esc>:wq!<cr>  
nnoremap <C-d> :wq!<cr>
" Discard changes and quit nvim
inoremap <C-q> <esc>:qa!<cr>               
nnoremap <C-q> :qa!<cr>


"Autocommands
if !exists('autocommands_loaded')
  let autocommands_loaded=1
  au BufWritePost init.vim source ~/.config/nvim/init.vim
  au BufWritePost *.tex,*.bib !xelatex 'main.tex' 
endif

" neotex settings
filetype plugin on
set shellslash
let g:neotex_enabled=1
let g:neotex_delay=1000
let g:neotex_subfile=1
let g:tex_flavor='latex'
